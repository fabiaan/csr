FROM node
COPY . /var/www
WORKDIR /var/www
RUN npm install
ENV PORT 6060
EXPOSE 6060
ENTRYPOINT [ "npm", "start" ]